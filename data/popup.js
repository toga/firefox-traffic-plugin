/*
 * CSN Traffic Plugin
 * 
 * Copyright 2013 Tobias Gall <tobias.gall@informatik.tu-chemnitz.de>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. * 
 * 
 */

self.port.on("update", function(data) {
	
	console.log("popup");
	var csn_data = data;
			
	if(!$.isEmptyObject(csn_data)) {
		
		var updated = csn_data.updated;
		var happy_total = csn_data.happyhour.total;
		var happy_up = csn_data.happyhour.upload;
		var happy_down = csn_data.happyhour.download;
		var normal_total = csn_data.normal.total;
		var normal_up = csn_data.normal.upload;
		var normal_down = csn_data.normal.download;			
						
		var Zeit = new Date();
		Zeit.setTime(csn_data.updated*1000);
						
		var happyhour = true;
		if(parseInt(Zeit.getHours()) >= 8) {
			happyhour = false;
		}
						
		var min = Zeit.getMinutes();
		if(min < 10)
			min = "0" + min;
						
		$("#updated strong").html(Zeit.getHours() + ":" + min);
						
		if(!happyhour) {
			$("#normal_total_tr").addClass("active");
			$("#happy_total_tr").removeClass("active").removeClass("success");
			if(normal_total < 6000) {
				$("#normal_total_tr").addClass("success");
			}
			else if(normal_total < 9000)  {
				$("#normal_total_tr").addClass("warning");
			}
			else {
				$("#normal_total_tr").addClass("danger");
			}
			if(normal_up < 6000) {
				$("#normal_up_tr").addClass("success");
			}
			else if(normal_up < 9000)  {
				$("#normal_up_tr").addClass("warning");
			}
			else {
				$("#normal_up_tr").addClass("danger");
			}
			if(normal_down < 6000) {
				$("#normal_down_tr").addClass("success");
			}
			else if(normal_down < 9000)  {
				$("#normal_down_tr").addClass("warning");
			}
			else {
				$("#normal_down_tr").addClass("danger");
			}				
		}
		else {
			$("#normal_total_tr").removeClass("active")
			.removeClass("success")
			.removeClass("warning")
			.removeClass("danger");
			
			$("#normal_down_tr").removeClass("active")
			.removeClass("success")
			.removeClass("warning")
			.removeClass("danger");
			
			$("#normal_up_tr").removeClass("active")
			.removeClass("success")
			.removeClass("warning")
			.removeClass("danger");
			
			$("#happy_total_tr").addClass("active success");
		}
					
		$("#normal_up").html(normal_up);
		$("#normal_down").html(normal_down);
		$("#normal_total").html(normal_total);
					
		$("#happy_up").html(happy_up);
		$("#happy_down").html(happy_down);
		$("#happy_total").html(happy_total);
	}
	else {
		console.log("empty csn_data");
		//$(".container").html("An Error occured while getting your Data").addClass("alert alert-danger");
	}
});
