var self = require("self");
var widgets = require("sdk/widget");
var tabs = require("sdk/tabs");
var panels = require("sdk/panel");
var notifications = require("sdk/notifications");
var Request = require("sdk/request").Request;
var tmr = require('timer');
var ss = require("sdk/simple-storage");

ss.storage.csn = null;


var panel = panels.Panel({
	width: 310,
	height: 370,
	contentScriptFile:[self.data.url("js/jquery-2.0.3.min.js"), self.data.url("js/bootstrap.min.js"), self.data.url("popup.js")],
	contentURL: self.data.url("popup.html")
});

var widget = widgets.Widget({
	id: "CSN Traffic Plugin",
	label: "CSN Traffic Plugin",
	contentURL: self.data.url("csn_icon-38.png"),
	panel: panel
});

autorefreshdata();

function autorefreshdata() {
	console.log("autorefresh");
	var intervalID;
	var freqSecs = 300; // alle 5 minuten
	intervalID = tmr.setInterval(RepeatCall, freqSecs*1000 );
		
	var csn_data = refreshdata();
		
	function RepeatCall() {
		
		console.log("RepeatCall");
					
		csn_data = refreshdata();
					
		var updated = csn_data.updated;
		var normal_total = csn_data.normal.total;
							
		var Zeit = new Date();
		Zeit.setTime(csn_data.updated*1000);
					
		var happyhour = true;
		if(Zeit.getHours() >= 8) {
			happyhour = false;
		}
						
		if(normal_total > 9000) {
			myAlert("Traffic Warning", "You have reached " + normal_total/100 + "% of your Trafficlimit");
		}
	};
};

function refreshdata() {

	var CSNAPI = "https://www.csn.tu-chemnitz.de/api/";
	var api_key = require('sdk/simple-prefs').prefs['csn_api_key'];
	
	if(api_key) {
		console.log(CSNAPI+api_key);
		var csn_data = ss.storage.csn;
		var csn_req = Request({
			url: CSNAPI+api_key,
			onComplete: function (response) {
				console.log("response: "+ response.text);
				csn_data = JSON.parse(response.text);
				ss.storage.csn = csn_data;
				panel.port.emit("update", csn_data);
			}
		}).get();
		return csn_data;
	}
	else {
		console.log("no api key");
		myAlert("Warning", "Please Enter your API Key");
		panel.port.emit("update", ss.storage.csn);
		return null;
	}
};

function myAlert(title, msg) {
	title = 'CSN Traffic: ' + title;
	notifications.notify({
		title: title,
		text: msg,
		iconURL: self.data.url("csn_icon-128.png")
	});
};

require("sdk/simple-prefs").on("csn_api_key", function() {
	refreshdata();
});
